import 'dart:io';

import 'package:alfred/alfred.dart';
import 'package:args/args.dart';
import 'package:jaguar/jaguar.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as shelf_io;

// import 'conduit_server.dart';

enum ServerType { alfred, conduit, http, jaguar, shelf }

var address = 'localhost';
var port = 8080;
var staticResponse = true;

void main(List<String> arguments) async {
  const compressedFlag = 'compress';
  const staticStringFlag = 'static';
  const typeFlag = 'type';
  const addressFlag = 'address';
  const portFlag = 'port';

  final parser = ArgParser()
    ..addFlag(compressedFlag)
    ..addFlag(staticStringFlag)
    ..addOption(typeFlag,
        allowed: ServerType.values.map((e) => e.name), defaultsTo: 'http')
    ..addOption(addressFlag,
        help: 'Local address for connection', defaultsTo: 'localhost')
    ..addOption(portFlag, help: 'Port for connection', defaultsTo: '8080');

  final argResults = parser.parse(arguments);
  final compressed = argResults[compressedFlag] as bool;
  staticResponse = argResults[staticStringFlag] as bool;
  final serverType = ServerType.values.byName(argResults[typeFlag]);
  address = argResults[addressFlag];
  port = int.parse(argResults[portFlag]);

  switch (serverType) {
    case ServerType.alfred:
      _runAlfredServer();
      break;
    case ServerType.http:
      _runDeprecatedHttpServer();
      break;
    case ServerType.jaguar:
      _runJaguarServer();
      break;
    case ServerType.shelf:
      _runShelfServer(compressed);
      break;
    case ServerType.conduit:
      // runConduitServer(address, port);
      break;
  }
}

// Deprecated HTTP Server Implementations
void _runDeprecatedHttpServer() {
  print('Serving with deprecated http_server at http://$address:$port '
      '${staticResponse ? "static" : "dynamic"} response');

  HttpServer.bind(address, port).then((server) {
    server.listen(staticResponse
        ? (HttpRequest request) {
            request.response.write('Hello World!\n');
            request.response.close();
          }
        : (HttpRequest request) {
            request.response
                .write('The time is ${DateTime.now().toIso8601String()}\n');
            request.response.close();
          });
  });
}

// Shelf Server Implementations
void _runShelfServer(bool compressed) async {
  var handler = const shelf.Pipeline().addHandler(staticResponse
      ? (request) => shelf.Response.ok('Hello World!\n')
      : (request) => shelf.Response.ok(
          'The time is ${DateTime.now().toIso8601String()}\n'));

  var server = await shelf_io.serve(handler, address, port);
  server.autoCompress = compressed;

  print('Serving with Shelf at http://${server.address.host}:${server.port}'
      ', ${compressed ? "with" : "without"} compression '
      '${staticResponse ? "static" : "dynamic"} response');
}

// Jaguar Implementations
void _runJaguarServer() async {
  print('Serving with Jaguar at http://$address:$port '
      '${staticResponse ? "static" : "dynamic"} response');
  final server = Jaguar();
  server.get(
      '/',
      staticResponse
          ? (context) => 'Hello World\n'
          : (context) => 'The time is ${DateTime.now().toIso8601String()}\n');
  await server.serve();
}

// Alfred
void _runAlfredServer() async {
  final server = Alfred(logLevel: LogType.error);
  server.get(
      '/',
      staticResponse
          ? (req, res) => 'Hello World\n'
          : (req, res) => 'The time is ${DateTime.now().toIso8601String()}\n');
  await server.listen(port, address);
  print('Serving with Alfred at http://$address:$port '
      '${staticResponse ? "static" : "dynamic"} response');
}
