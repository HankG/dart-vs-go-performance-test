import 'package:conduit/conduit.dart' as conduit;

import 'dartserver.dart';

void runConduitServer(String address, int port) async {
  final app = conduit.Application<BenchmarkChannel>()
    ..options.address = address
    ..options.port = port;

  await app.startOnCurrentIsolate();

  print('Serving with Conduit at ${app.options.address}:${app.options.port} '
      '${staticResponse ? "static" : "dynamic"} response');
}

class BenchmarkChannel extends conduit.ApplicationChannel {
  @override
  conduit.Controller get entryPoint {
    final router = conduit.Router();

    router.route("/").linkFunction(staticResponse
        ? (request) async => conduit.Response.ok('Hello World!\n')
        : (request) async => conduit.Response.ok(
            'The time is ${DateTime.now().toIso8601String()}\n'));

    return router;
  }
}
