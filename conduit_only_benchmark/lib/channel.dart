import 'package:conduit_only_benchmark/conduit_only_benchmark.dart';

class ConduitOnlyBenchmarkChannel extends ApplicationChannel {
  @override
  Controller get entryPoint {
    final router = Router();

    router.route("/").linkFunction((request) async => Response.ok(
        'The time is ${DateTime.now().toIso8601String()}\n'));

    return router;
  }
}
