import 'package:conduit_only_benchmark/conduit_only_benchmark.dart';

Future main() async {
  final app = Application<ConduitOnlyBenchmarkChannel>()
    ..options.configurationFilePath = "config.yaml";

  await app.startOnCurrentIsolate();

  print("Application started on port: ${app.options.port}.");
  print("Use Ctrl-C (SIGINT) to stop running the application.");
}
