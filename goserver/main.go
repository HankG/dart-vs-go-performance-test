package main

import (
	"flag"
	"fmt"
	"net/http"
	"runtime"
	"time"
)

func main() {
	var singleCore bool
	var staticString bool
	flag.BoolVar(&singleCore, "single", false, "Run on a single core only")
	flag.BoolVar(&staticString, "static", false, "Use static string")
	flag.Parse()

	if singleCore {
		runtime.GOMAXPROCS(1)
	}

	fmt.Print("Go Server Listening on localhost:8080 ")
	if singleCore {
		fmt.Print("with single core ")
	} else {
		fmt.Print("with max cores ")
	}

	if staticString {
		fmt.Println("with static response")
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "Hello World!")
		})
	} else {
		fmt.Println("with dynamic response")
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "The time is: "+time.Now().Format(time.RFC3339))
		})
	}

	http.ListenAndServe(":8080", nil)
}
